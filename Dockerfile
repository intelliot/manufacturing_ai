# FROM python:3.6-alpine
FROM ubuntu:focal as ubuntu

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

#Set timezone to avoid prompt on apt update
ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update -y && apt-get install -y --no-install-recommends --no-install-suggests python3 python3-pip iputils-ping ffmpeg libsm6 libxext6

COPY requirements.txt /usr/src/app/

ADD tensorflow-2.8.0-cp38-cp38-linux_x86_64.whl /usr/src/app/
RUN pip3 install --no-cache-dir -r requirements.txt
ADD models_hdf5/ /usr/src/app/swagger_server/models

COPY swagger_server /usr/src/app/swagger_server

EXPOSE 8080

# ENTRYPOINT ["python3"]

# CMD ["-m", "swagger_server"]

CMD python3 -m swagger_server