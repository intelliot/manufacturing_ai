# Camera AI App

This app supports the automation of the IntellIoT Manufacturing use case in terms of providing the following functionalities based on processing camera images with bird's eye view of the Workpiece Storage, Laser Engraver, and Milling Machine:
- Identify cross-hair markers placed on the surfaces [![Cross-hair marker](https://www.ebay.com/itm/142936589628 "Cross-hair marker")](https://www.ebay.com/itm/142936589628 "Cross-hair marker")
- Compute the engraving area, which is the area of the largest circle that can be placed inside a wooden workpiece
```json
    ComputeEngravingAreaResponse:
      type: array
      items:
        type: object
        properties: 
          xcoordinate:
            type: number
            description: "measured from the upper left marker (the most left for all storage places)"
          ycoordinate:
            type: number
            description: "measured from the upper left marker"
          radius:
            type: number
          confidence:
            type: number
            format: double
```
- Calculate the grabspot for a given workpiece
```json
    GetGrabspotResponse:
      type: array
      items:
        type: object
        properties: 
          xcoordinate:
            type: number
            format: double
          ycoordinate:
            type: number
            format: double
          angle:
            type: number
            format: double
          confidence:
            type: number
            format: double
```

### Installation
This installation needs python 3.8 and a windows operating system. If the operating system differs, the corresponding tensorflow version should be installed. The required dependencies are given in the *requirements.txt* file. 
```python
pip install -r requirements.txt
```
Additionally, you have to add the hdf5 model to `models_hdf5/workpieceStorage_marker_model_small.hdf5` and if needed the wheel to `tensorflow-2.8.0-cp38-cp38-linux_x86_64.whl` in the workdir root. To obtain the hdf5 model, get in touch with the [contact person](#contact). If the required dependencies are satisfied, run the following command to start the server.
```python
python -m swagger_server
```
### HTTP call to obtain delivery information:
```
http://[IP:PORT]/AI_Service/compute_engravingArea?storageId=[ID]&cameraHostname=[HOST]&cameraId=[CAMERA]
```
- [IP:PORT] - IP address and the port of the AI service that is run on the edge server (e.g. "127.0.0.1:8080").
- [ID] - ID of the storage areas. Using negative number returns a visualization for debugging purposes.
	- Workpiece Storage: ID $\in$ \{1, 2, 3, 4\}. 
	- Laser Engraver: ID = 1
	- Milling Machine: ID = 1
	- For testing purposes, use ID$\in$\{1,2,3,4,5,6\} with testing keywords given below
- [HOST] - IP address and the port of the RPi camera (e.g. "127.0.0.1:8080"). 
	- In the Manufacturing setting, currently we are using HOST$\in$\{camera-storage.fritz.box, camera-milling.fritz.box, camera-engraver.fritz.box\}
	- Use HOST = testimages for testing with the [test image](edge_ai_delivery/swagger_server/models/test_image.jpg) instead of contacting the camera.
- [CAMERA] - Defines the camera that connects to the three locations
	- CAMERA$\in$\{workpieceStorage, millingMachine, laserEngraver\}
	- Use CAMERA = testimages for testing

**Note**: For the testing purposes, the server can generate several pop up windows visualizing camera image and its manupilations. By changing `SHOW_PLOTS` to `False` on the line 15 in the file *swagger_server/models/\_\_init\_\_.py*, the popup windows can be avoided. This is essential if the server is running on MAC OS.


### Dataset
Dataset contains the images of crosshair markers as input while the output is the corresponding masked 
images indicating the crosshair by white pixels and the rest as the black pixels.
This dataset is created by augmenting a selected set of [images](Dataset/resources) from the demo setup.
During the augmentation, the parts of the original images and corresponding masks are extracted, which are then
distorted by scaling, skewing, rotating, and flipping.
The [inputs](Dataset/images) are the distorted images and the [outputs](Dataset/labels) are the 
corresponding distorted masks.

### Contact
Sumudu Samarakoon, 
Assistant Professor, 
ICON, CWC,
6G Flagship,
University of Oulu,
Finland. 
<sumudu.samarakoon@oulu.fi>

### Contributors
- https://github.com/Abdulmomen96
- https://github.com/mcsalinda
- https://github.com/mdweera
- https://github.com/spsamare
- https://www.facebook.com/suranga.prasad.14
