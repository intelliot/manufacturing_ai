from tensorflow import keras
import os
import configparser
import urllib.error

import swagger_server.init_marker_detection as detector

MARKER_MODEL_INIT = None

"""
# This is for local testing
if os.path.exists("./swagger_server/models/workpieceStorage_marker_model.hdf5"):
    MARKER_MODEL_INIT = keras.models.load_model("./swagger_server/models/workpieceStorage_marker_model.hdf5")
else:
    MARKER_MODEL_INIT = keras.models.load_model("workpieceStorage_marker_model.hdf5")
"""

""""""
# This is for the implementation
if os.path.exists("./swagger_server/models/workpieceStorage_marker_model_small.hdf5"):
    json_model_file = open("./swagger_server/models/workpieceStorage_marker_model_small.json", 'r')
    json_model = json_model_file.read()
    json_model_file.close()
    MARKER_MODEL_INIT = keras.models.model_from_json(json_model)
    MARKER_MODEL_INIT.load_weights("./swagger_server/models/workpieceStorage_marker_model_small.hdf5")
    MARKER_MODEL_INIT.compile(loss='binary_crossentropy', optimizer='adam', metrics=['loss', 'accuracy'])
elif os.path.exists("./workpieceStorage_marker_model_small.hdf5"):
    json_model_file = open("./workpieceStorage_marker_model_small.json", 'r')
    json_model = json_model_file.read()
    json_model_file.close()
    MARKER_MODEL_INIT = keras.models.model_from_json(json_model)
    MARKER_MODEL_INIT.load_weights("./workpieceStorage_marker_model_small.hdf5")
    MARKER_MODEL_INIT.compile(loss='binary_crossentropy', optimizer='adam', metrics=['loss', 'accuracy'])
else:
    json_model_file = open("models/workpieceStorage_marker_model_small.json", 'r')
    json_model = json_model_file.read()
    json_model_file.close()
    MARKER_MODEL_INIT = keras.models.model_from_json(json_model)
    MARKER_MODEL_INIT.load_weights("models/workpieceStorage_marker_model_small.hdf5")
    MARKER_MODEL_INIT.compile(loss='binary_crossentropy', optimizer='adam', metrics=['loss', 'accuracy'])
""""""
SHOW_PLOTS = False

config = configparser.ConfigParser()
try:
    if os.path.exists("./swagger_server/config.ini"):
        config.read("./swagger_server/config.ini")
    elif os.path.exists("./config.ini"):
        config.read("./config.ini")
    else:
        config.read("config.ini")

    CAMERA_HOSTNAMES = config['camera hostnames']
    CAMERA_URLS = config['camera urls']
    CALIBRATION = config['calibration']
except:
    print("config.ini is missing or incomplete.")

""""""
MARKERS_WORKPIECE_STORAGE = []
MARKERS_LASER_ENGRAVER = []
MARKERS_MILLING_MACHINE = []
if CALIBRATION['initialCalibrate'] == "1":
    try:
        MARKERS_WORKPIECE_STORAGE = detector.find_markers_workpiece_storage(
            link=CAMERA_HOSTNAMES['workpieceStorage'] + CAMERA_URLS['workpieceStorage'],
            model=MARKER_MODEL_INIT)
        print("Marker locations of Workpiece Storage are now fixed...")
        """
        MARKERS_LASER_ENGRAVER = detector.find_markers_laser_engraver(
            link=CAMERA_HOSTNAMES['laserEngraver'] + CAMERA_URLS['laserEngraver'],
            model=MARKER_MODEL_INIT)
        print("Marker locations of Laser Engraver are now fixed...")

        MARKERS_MILLING_MACHINE = detector.find_markers_laser_engraver(
            link=CAMERA_HOSTNAMES['millingMachine'] + CAMERA_URLS['millingMachine'],
            model=MARKER_MODEL_INIT)
        print("Marker locations of Milling Machine are now fixed...")
        """
    except ValueError as e:
        print('error_description: ' + str(e.args[0]))
    except AssertionError as e:
        print('error_description: ' + str(e.args[0][1]))
    except urllib.error.URLError as e:
        print('error_description: No URL found with the Camera Hostname')
""""""
