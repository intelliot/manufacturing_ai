from swagger_server.models.compute_engraving_area_response import ComputeEngravingAreaResponse  # noqa: E501
from swagger_server.models.get_grabspot_response import GetGrabspotResponse  # noqa: E501
from swagger_server.models.error_response import ErrorResponse  # noqa: E501

from swagger_server.models.area_finder_workpieceStorage import find_area as workpieceStorageArea
from swagger_server.models.area_finder_laserEngraver import find_area as laserEngraverArea
from swagger_server.models.area_finder_millingMachine import find_area as millingMachineArea
from swagger_server.models.calculate_grabspot_workpieceStorage import find_grab_spot as workpieceStorageGrab
from swagger_server.models.calculate_grabspot_laserEngraver import find_grab_spot as laserEngraverGrab
from swagger_server.models.calculate_grabspot_millingMachine import find_grab_spot as millingMachineGrab

import cv2 as cv
import numpy as np
import urllib.request
import urllib.error
import os

from swagger_server.__init__ import CAMERA_HOSTNAMES, CAMERA_URLS

STORAGES = ['workpieceStorage', 'millingMachine', 'laserEngraver', 'testImages']
STORAGES_MAX = [4, 1, 1, 6]


def get_image(camera_type):  # get_image(hostname="", id="1"):
    """
    if hostname == "":
        url_ = "https://i.postimg.cc/vbFq0CLq/new-objective-4.jpg?dl=1"
        url_response = urllib.request.urlopen(url_)
        img_ = cv.imdecode(np.array(bytearray(url_response.read()), dtype=np.uint8), -1)
        return img_
    else:
        url_ = "http://" + hostname + "/picture/" + id + "/current/"
        url_response = urllib.request.urlopen(url_)
        return cv.imdecode(np.array(bytearray(url_response.read()), dtype=np.uint8), -1)
    """
    url_ = "http://" + CAMERA_HOSTNAMES[camera_type] + CAMERA_URLS[camera_type]  # url_ = "http://" + hostname + CAMERA_URLS[id]
    # print(url_)
    url_response = urllib.request.urlopen(url_)
    return cv.imdecode(np.array(bytearray(url_response.read()), dtype=np.uint8), -1)


def get_test_image(id_="1"):
    if int(id_) < 5:
        id_ = "1"
    return cv.imread('models/test_' + str(id_) + '.jpg')


def call_grab_spot_function(camera_type, storage_id, img_input, return_image=False):
    if camera_type == 'workpieceStorage':
        return workpieceStorageGrab(storage_id, img_input, return_image)
    elif camera_type == 'laserEngraver':
        return laserEngraverGrab(img_input, return_image)
    elif camera_type == 'millingMachine':
        return millingMachineGrab(img_input, return_image)
    else:
        return 'error'


def call_engrave_area_function(camera_type, storage_id, img_input, return_image=False):
    if camera_type == 'workpieceStorage':
        return workpieceStorageArea(storage_id, img_input, return_image)
    elif camera_type == 'laserEngraver':
        return laserEngraverArea(img_input, return_image)
    elif camera_type == 'millingMachine':
        return millingMachineArea(img_input, return_image)
    else:
        return 'error'


def a_i_service_compute_engraving_area_get(storage_id, camera_hostname, camera_id):  # noqa: E501
    """request AI to compute area of given four woodpieces image

    Request AI to compute # noqa: E501

    :param storage_id: ID of the position of the workpiece to compute engraving area.
    Currently, defined IDs: integer values: workpieceStorage [1-4], millingMachine [1], laserEngraver [1]
    :type storage_id: str
    :param camera_hostname: host name of the camera to get the image from.
    Currently, defined Hostnames: camera-storage.fritz.box, camera-milling.fritz.box, camera-engraver.fritz.box
    :type camera_hostname: str
    :param camera_id: ID for the type of the camera.
    Currently, defined camera_id: workpieceStorage, millingMachine, laserEngraver
    :type camera_id: str

    :rtype: ComputeEngravingAreaResponse
    """
    try:
        assert camera_id in STORAGES, (405, "Invalid Camera ID")
        camera_type_index = STORAGES.index(camera_id)
        assert CAMERA_HOSTNAMES[camera_id] == camera_hostname, (405, "Mismatch in camera hostname and ID")
        assert 0 < np.abs(int(storage_id)) < STORAGES_MAX[camera_type_index] + 1, (405, "Invalid Storage ID")

        if camera_id == "testImages":
            img = get_test_image(id_=np.abs(int(storage_id)))
            assert img is not None, (405, "Test image is missing")
            # camera_hostname = STORAGES[np.maximum(np.abs(int(storage_id)) - 4, 0)]
            camera_id = STORAGES[np.maximum(np.abs(int(storage_id)) - 4, 0)]
        else:
            img = get_image(camera_type=camera_id)  # img = get_image(hostname=camera_hostname, id=camera_id)

        if int(storage_id) < 0:  # for testing only
            storage_id = str(-1 * int(storage_id))
            img_response = call_engrave_area_function(camera_type=camera_id, storage_id=storage_id, img_input=img,
                                                      return_image=True)
            _, img_encoded = cv.imencode('.jpg', img_response)
            import io
            io_buf = io.BytesIO(img_encoded)
            from flask import send_file
            return send_file(
                io_buf,
                mimetype='image/jpeg',
                as_attachment=True,
                attachment_filename='0.jpg')
        else:
            x_mm, y_mm, radius_mm, confidence = call_engrave_area_function(camera_type=camera_id,
                                                                           storage_id=storage_id, img_input=img)

            response = {
                "xcoordinate": x_mm,
                "ycoordinate": y_mm,
                "radius": radius_mm,
                "confidence": confidence
            }
            return ComputeEngravingAreaResponse.from_dict(dikt=response)
    except AssertionError as e:
        response = {
            'error_code': e.args[0][0],
            'error_description': e.args[0][1]
        }
        # return ErrorResponse.from_dict(dikt=response)
        return ErrorResponse(error_code=response['error_code'], error_description=response['error_description'])
    except ValueError:
        response = {
            'error_code': 400,
            'error_description': 'Bad Storage ID'
        }
        return ErrorResponse(error_code=response['error_code'], error_description=response['error_description'])
    except urllib.error.URLError as e:
        response = {
            'error_code': 400,
            'error_description': 'Bad Camera URL'
        }
        # return ErrorResponse.from_dict(dikt=response)
        return ErrorResponse(error_code=response['error_code'], error_description=response['error_description'])


def a_i_service_get_grabspot_get(storage_id, camera_hostname, camera_id):  # noqa: E501
    """request AI to compute grab spot of given woodpiece ID

    Calls for the grabspot # noqa: E501

    :param storage_id: ID of the position of the workpiece to compute engraving area.
    Currently, defined IDs: integer values: workpieceStorage [1-4], millingMachine [1], laserEngraver [1]
    :type storage_id: str
    :param camera_hostname: host name of the camera to get the image from.
    Currently, defined Hostnames: camera-storage.fritz.box, camera-milling.fritz.box, camera-engraver.fritz.box
    :type camera_hostname: str
    :param camera_id: ID for the type of the camera.
    Currently, defined camera_id: workpieceStorage, millingMachine, laserEngraver
    :type camera_id: str

    :rtype: GetGrabspotResponse
    """
    try:
        assert camera_id in STORAGES, (405, "Invalid Camera ID")
        camera_type_index = STORAGES.index(camera_id)
        assert CAMERA_HOSTNAMES[camera_id] == camera_hostname, (405, "Mismatch in camera hostname and ID")
        assert 0 < np.abs(int(storage_id)) < STORAGES_MAX[camera_type_index] + 1, (405, "Invalid Storage ID")

        if camera_id == "testImages":
            img = get_test_image(np.abs(int(storage_id)))
            assert img is not None, (405, "Test image is missing")
            camera_id = STORAGES[np.maximum(np.abs(int(storage_id)) - 4, 0)]
        else:
            img = get_image(camera_type=camera_id)

        if int(storage_id) < 0:  # for testing only
            storage_id = str(-1 * int(storage_id))
            img_response = call_grab_spot_function(camera_type=camera_id, storage_id=storage_id, img_input=img,
                                                   return_image=True)
            _, img_encoded = cv.imencode('.jpg', img_response)
            import io
            io_buf = io.BytesIO(img_encoded)
            from flask import send_file
            return send_file(
                io_buf,
                mimetype='image/jpeg',
                as_attachment=True,
                attachment_filename='0.jpg')
        else:
            x_mm, y_mm, angle_deg, confidence = call_grab_spot_function(camera_type=camera_id,
                                                                        storage_id=storage_id,
                                                                        img_input=img)
            response = {
                "xcoordinate": x_mm,
                "ycoordinate": y_mm,
                "angle": angle_deg,
                "confidence": confidence
            }

            return GetGrabspotResponse.from_dict(response)
    except ValueError as e:
        response = {
            'error_code': 400,
            'error_description': e.args[0]
        }
        return ErrorResponse(error_code=response['error_code'], error_description=response['error_description'])
    except AssertionError as e:
        response = {
            'error_code': e.args[0][0],
            'error_description': e.args[0][1]
        }
        return ErrorResponse(error_code=response['error_code'], error_description=response['error_description'])
    except urllib.error.URLError as e:
        response = {
            'error_code': 400,
            'error_description': 'No URL found with the Camera Hostname'
        }
        return ErrorResponse(error_code=response['error_code'], error_description=response['error_description'])
