from swagger_server.models.image_process import test_generator, save_result

import cv2 as cv
import numpy as np
import urllib.request
import urllib.error
from polylabel import polylabel


def get_image(link):
    url_ = "http://" + link
    url_response = urllib.request.urlopen(url_)
    return cv.imdecode(np.array(bytearray(url_response.read()), dtype=np.uint8), -1)


def find_markers_workpiece_storage(link, model):
    img_input = get_image(link=link)

    img_input = resize_workpiece_storage(img_input)
    img_input = cv.cvtColor(img_input, cv.COLOR_BGR2GRAY)

    size = 300  # 600
    points = [
        [55, 386], [492, 371], [941, 358], [1398, 342], [1856, 327],
        [1884, 789], [1416, 803], [955, 814], [498, 826], [57, 832]
    ]
    """
    points_1920 = [
        [55, 386], [492, 371], [941, 358], [1398, 342], [1856, 327],
        [1884, 789], [1416, 803], [955, 814], [498, 826], [57, 832]
    ]   
    points_1280 = [
        [157, 1008], [1055, 1029], [1956, 1044], [2852, 1067], [3736, 1086],
        [3716, 1977], [2834, 1966], [1936, 1950], [1035, 1934], [142, 1907]
    ]
    points = [
        [20, 756], [860, 756], [1700, 756], [2540, 756], [3380, 756],
        [20, 1600], [860, 1600], [1700, 1600], [2540, 1600], [3380, 1600]
    ]   
    """
    return run_marker_detection_init(img_input=img_input, size=size, points=points, model=model)


def find_markers_laser_engraver(link, model):
    img_input = get_image(link=link)

    img_input = resize_laser_engraver(img_input)
    img_input = cv.cvtColor(img_input, cv.COLOR_BGR2GRAY)

    size = 600
    points = [
        [214, 296], [752, 281], [753, 824], [234, 812]
    ]
    return run_marker_detection_init(img_input=img_input, size=size, points=points, model=model)


def find_markers_milling_machine(link, model):
    img_input = get_image(link=link)

    img_input = resize_milling_machine(img_input)
    img_input = cv.cvtColor(img_input, cv.COLOR_BGR2GRAY)

    size = 300
    points = [
        [409, 169], [1015, 153], [1034, 457], [410, 474]
    ]
    return run_marker_detection_init(img_input=img_input, size=size, points=points, model=model)


def run_marker_detection_init(img_input, size, points, model):
    # splitting
    num_markers = len(points)
    img_segments = []
    segment_origin = []
    for idx in range(num_markers):
        img_segments.append(
            img_input[
                max(int(points[idx][1] - size / 2), 0): min(int(points[idx][1] + size / 2), img_input.shape[0]),
                max(int(points[idx][0] - size / 2), 0): min(int(points[idx][0] + size / 2), img_input.shape[1])]
        )
        segment_origin.append(
            [max(int(points[idx][1] - size / 2), 0), max(int(points[idx][0] - size / 2), 0)])
    # visualize
    """
    if GENERATE_IMAGE is True:
        combined_segments = np.vstack((
            np.hstack(tuple(map(tuple, img_segments[:5]))),
            np.hstack(tuple(map(tuple, img_segments[5:])))
        ))
        visualize(combined_segments, title="Original segments")
    """
    # converting images into marker detected images
    """"""
    input_images = test_generator(img_segments)
    output_images = model.predict(input_images, len(img_segments), verbose=1)
    images_marker = save_result(output_images, flag_multi_class=False)
    # np.save('marker_images.npy', images_marker)
    """"""
    # extracting location of the centre_ of the markers
    centers_local = []
    centers_global = []
    # images_marker = np.load('marker_images.npy')
    # print(images_marker)
    for indMarkers in range(len(images_marker)):
        segment_input = images_marker[indMarkers]
        segment_input = np.transpose(segment_input)
        segment_input = cv.normalize(segment_input, None, 0, 255, cv.NORM_MINMAX, cv.CV_8U)
        segment_input = cv.resize(segment_input, img_segments[indMarkers].shape, interpolation=cv.INTER_AREA)
        # img = cv.rotate(img, cv.ROTATE_90_COUNTERCLOCKWISE)
        """
        filename = "marker_" + str(indMarkers) + ".png"
        matplotlib.image.imsave(filename, img, cmap='gray')
        """

        (contours_, _) = cv.findContours(segment_input, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        areas_ = []
        poly_approx_ = []
        for i, contour in enumerate(contours_):
            areas_.append(cv.contourArea(contour))
            perimeter = cv.arcLength(contour, True)
            poly_approx_.append(cv.approxPolyDP(contours_[i], perimeter * 0.001, True))
        assert len(areas_) > 0, (400, 'Missing marker(s) from the camera image')
        sorted_area_index = np.argsort(areas_)
        poly_ = np.squeeze(poly_approx_[sorted_area_index[-1]])
        if poly_.shape[0] > 2:
            centre_, radius_ = polylabel([poly_.tolist()], with_distance=True)
            c_local = [int(centre_[1]), int(centre_[0])]
            c_global = [c_local[0] + segment_origin[indMarkers][1],
                        c_local[1] + segment_origin[indMarkers][0]]
            centers_local.append(c_local)
            centers_global.append(c_global)
    # visualize
    """
    if GENERATE_IMAGE is True:
        img_plot = img_input.copy()
        for i in range(10):
            cv.drawMarker(img_plot, (centers_global[i][0], centers_global[i][1]), (255, 0, 0), thickness=2)
        visualize(img_plot, title="Marked full image")
    """

    marker_corners = update_markers(points=centers_global, num_markers=num_markers)
    assert len(marker_corners) > 0, (405, 'Missing marker(s) from detection')
    marker_radius = 0  # 2018 - 1955
    return marker_corners


def update_markers(points, num_markers):
    if num_markers == 10:
        markers = [
            [points[0], points[1], points[8], points[9]],  # corners in order TL, TR, BR, BL
            [points[1], points[2], points[7], points[8]],  # corners in order TL, TR, BR, BL
            [points[2], points[3], points[6], points[7]],  # corners in order TL, TR, BR, BL
            [points[3], points[4], points[5], points[6]]  # corners in order TL, TR, BR, BL
        ]
    elif num_markers == 4:
        markers = [
            [points[0], points[1], points[2], points[3]]  # corners in order TL, TR, BR, BL
        ]
    else:
        markers = []
    return markers


def resize_workpiece_storage(img_input):
    width = 1920  # 4056
    height = 1080  # 3040
    dim = (width, height)
    # resize image
    return cv.resize(img_input, dim, interpolation=cv.INTER_AREA)


def resize_laser_engraver(img_input):
    width = 1280
    height = 1024
    dim = (width, height)
    # resize image
    return cv.resize(img_input, dim, interpolation=cv.INTER_AREA)


def resize_milling_machine(img_input):
    width = 1280
    height = 960
    dim = (width, height)
    # resize image
    return cv.resize(img_input, dim, interpolation=cv.INTER_AREA)
