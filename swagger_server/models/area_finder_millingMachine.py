# import numpy as np
# import cv2 as cv
# from matplotlib import pyplot as plt
import math
# from swagger_server.models.image_process import test_generator, save_result
# from polylabel import polylabel
# from tensorflow import keras

# from swagger_server.__init__ import MARKER_MODEL_INIT
from swagger_server.models.common_functions import *

MARKER_GAP = 140
TABLE_OFFSET = 0
CROP_OFFSET_SCALE = .25


def get_markers(img_input):
    # splitting
    size = 300

    points = [
        [409, 169], [1015, 153], [1034, 457], [410, 474]
    ]
    return run_marker_detection(img_input=img_input, size=size, points=points)


def find_area(img_input, return_image=False):
    # variables
    # visualize(img_input)
    generate_image = SHOW_PLOTS or return_image

    # storage_id = int(storage_id_str) - 1
    storage_id = 0
    kernel_dilation = np.ones((10, 10), np.uint16)  # dilation kernel_dilation

    img_input = resize_to_predefined(img_input)
    img_gray = cv.cvtColor(img_input, cv.COLOR_BGR2GRAY)

    # get marker locations
    markers, delta = get_markers(img_gray)

    """
    if generate_image is True:
        img_plot = img_input.copy()
        for i in range(4):
            cv.drawMarker(img_plot, (markers[storage_id][i][0], markers[storage_id][i][1]), (0, 0, 255), thickness=4)
        visualize(img_plot, title="Markers of selected storage")
    """

    # select the correct storage area
    # Here, specify the input coordinates for corners in the order of TL, TR, BR, BL
    marker_rectangle = np.float32([markers[0][0], markers[0][1],
                                   markers[0][2], markers[0][3]])
    # print(marker_rectangle)
    # delta = 0  # += 20

    # treat camera distortions ---------------------------
    # read input width and height
    height_img, width_img = img_gray.shape[:2]
    # get top and left dimensions and set to corner_coordinates dimensions of target rectangle
    width = round(math.hypot(marker_rectangle[0, 0] - marker_rectangle[1, 0],
                             marker_rectangle[0, 1] - marker_rectangle[1, 1]))
    height = round(math.hypot(marker_rectangle[0, 0] - marker_rectangle[3, 0],
                              marker_rectangle[0, 1] - marker_rectangle[3, 1]))
    # top left coordinates
    x_top_left, y_top_left = marker_rectangle[0, 0], marker_rectangle[0, 1]
    # corner_coordinates coordinates of corners in order TL, TR, BR, BL
    corner_coordinates = np.float32([
        [x_top_left, y_top_left], [x_top_left + width - 1, y_top_left],
        [x_top_left + width - 1, y_top_left + height - 1], [x_top_left, y_top_left + height - 1]
    ])
    # print(corner_coordinates)

    # compute perspective matrix
    perspective_matrix = cv.getPerspectiveTransform(marker_rectangle, corner_coordinates)
    # Perspective transformation setting and corner_coordinates size is the same as the input image size ???
    img_straighten = cv.warpPerspective(img_gray, perspective_matrix, (width_img, height_img), cv.INTER_LINEAR,
                                        borderMode=cv.BORDER_CONSTANT, borderValue=(0, 0, 0))
    """
    if generate_image is True:
        img_straighten_color = cv.warpPerspective(img_input, perspective_matrix, (width_img, height_img),
                                                  cv.INTER_LINEAR, borderMode=cv.BORDER_CONSTANT, borderValue=(0, 0, 0))
        # visualize(img_straighten_color, title="Distortion correction")
    """

    new_markers = get_translated_points(markers[0], perspective_matrix)

    # filtering
    img_blurred = cv.bilateralFilter(img_straighten, 8, 25, 25)
    img_blurred_final = cv.GaussianBlur(img_blurred, (3, 3), 0)

    # fix brightness
    bright_sigma = 0.33
    image_median = np.median(img_blurred_final)
    if image_median > 191:  # light images
        brightness_tuner = 2
    elif image_median > 127:
        brightness_tuner = 1
    elif image_median < 63:  # dark
        brightness_tuner = 2
    else:
        brightness_tuner = 1
    brightness_lower = int(max(0, (1.0 - brightness_tuner * bright_sigma) * image_median))
    brightness_upper = int(min(255, (1.0 + brightness_tuner * bright_sigma) * image_median))

    # find edges
    edges = cv.Canny(img_blurred_final, brightness_lower, brightness_upper)
    img_dilated = cv.dilate(edges, kernel_dilation, iterations=1)

    # visualize(edges, title="Edge detection")
    """
    img_cropped = img_dilated[
                  :markers[storage_id][3][1],
                  markers[storage_id][3][0] + delta:
                  markers[storage_id][2][0] - delta
                  ]
    """
    width_cropped = np.minimum(new_markers[1][0] - new_markers[0][0], new_markers[2][0] - new_markers[3][0])

    crop_offset = int(CROP_OFFSET_SCALE * width_cropped)
    img_cropped = img_dilated[
                  :new_markers[3][1] + crop_offset,
                  new_markers[3][0]:new_markers[2][0]
                  ]

    # making a rectangle from markers
    # Calculate scaling from pixels to actual distances
    scale_factor = 2 * MARKER_GAP / width_cropped  # need to fix, 280 mm
    height_markers = np.minimum(new_markers[3][1] - new_markers[0][1], new_markers[2][1] - new_markers[1][1])
    height_cropped = int(round(img_cropped.shape[0] * width_cropped / (2 * height_markers)))
    new_offset = int(round(crop_offset * width_cropped / (2 * height_markers)))
    img_cropped = cv.resize(img_cropped, (width_cropped, height_cropped), interpolation=cv.INTER_AREA)
    markers_cropped = [
        [0, height_cropped - width_cropped // 2 - new_offset - 1],
        [width_cropped - 1, height_cropped - width_cropped // 2 - new_offset - 1],
        [0, height_cropped - new_offset - 1], [width_cropped - 1, height_cropped - new_offset - 1]
    ]

    img_morphed = cv.morphologyEx(img_cropped, cv.MORPH_CLOSE, kernel_dilation)
    # get contours
    (contours_all, _) = cv.findContours(img_morphed.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

    contours = {}
    contour_areas = []
    polygon_approx = []

    for index, contour in enumerate(contours_all):
        contour_areas.append(cv.contourArea(contour))
        perimeter = cv.arcLength(contour, True)
        # approximate to a polygon with given epsilon value
        polygon_approx.append(
            cv.approxPolyDP(contours_all[index], perimeter * 0.001, True))
        contours[index] = {
            "contour": contour,
            "area": contour_areas[index],
            "perimeter": perimeter,
            "polygon_approx": polygon_approx[index],
            "approxLen": len(polygon_approx)
        }

    # sorting area indexes
    sorted_indices_area = np.argsort(contour_areas)
    # print(len(polygon_approx[sorted_indices_area[-1]]), len(polygon_approx[sorted_indices_area[-2]]))

    # detecting weather there is inside edges

    # calculating radius and center
    poly = np.squeeze(polygon_approx[sorted_indices_area[-2]])  # hardcoded to be 2nd largest
    centre = [0., 0.]
    radius = 0.
    if poly.shape[0] > 2:
        centre, radius = polylabel([poly.tolist()], with_distance=True)

    # translate to the original coordinates
    # x_mm = storage_id * MARKER_GAP + pixel_to_mm(centre[0], ratio=scale_factor)
    # y_mm = pixel_to_mm(centre[1] - markers_cropped[0][1], ratio=scale_factor)
    x_mm = 2 * MARKER_GAP - pixel_to_mm(centre[0], ratio=scale_factor)
    y_mm = pixel_to_mm(markers_cropped[3][1] - centre[1], ratio=scale_factor)
    radius_mm = pixel_to_mm(radius, ratio=scale_factor)
    # fix the following
    confidence = compute_confidence(MARKER_GAP, MARKER_GAP + TABLE_OFFSET,
                                    pixel_to_mm(centre[0], ratio=scale_factor), y_mm, radius_mm)

    """"""
    # extracted contour, visualization only
    if generate_image is True:
        img_plot = img_input.copy()
        for i in range(4):
            cv.drawMarker(img_plot, (markers[storage_id][i][0], markers[storage_id][i][1]), (0, 0, 255), thickness=4)
        visualize(img_plot, title="Markers of selected storage")

        img_straighten_color = cv.warpPerspective(img_input, perspective_matrix, (width_img, height_img),
                                                  cv.INTER_LINEAR, borderMode=cv.BORDER_CONSTANT, borderValue=(0, 0, 0))
        visualize(img_straighten_color, title="Distortion correction")
        visualize(edges, title="Edge detection")

        image_visual = img_straighten_color[
                       :new_markers[3][1] + crop_offset,
                       new_markers[3][0]:new_markers[2][0]
                       ]
        image_visual = cv.resize(image_visual, (width_cropped, height_cropped), interpolation=cv.INTER_AREA)
        for i in range(4):
            cv.drawMarker(image_visual, (markers_cropped[i][0], markers_cropped[i][1]), (0, 0, 255), thickness=4)
        cv.circle(image_visual, (int(centre[0]), int(centre[1])), int(radius), (34, 139, 34), -1)
        cv.drawMarker(image_visual, (int(centre[0]), int(centre[1])), (0, 0, 255), thickness=4)
        # show information on image
        font = cv.FONT_HERSHEY_SIMPLEX
        str(np.round(x_mm, 2))
        cv.putText(image_visual, "x_mm: " + str(np.round(x_mm, 2)),
                   (10, height_cropped - 100), font, 1,
                   (255, 0, 255), 2, cv.LINE_AA)
        cv.putText(image_visual, "y_mm: " + str(np.round(y_mm, 2)),
                   (10, height_cropped - 70), font, 1,
                   (255, 0, 255), 2, cv.LINE_AA)
        cv.putText(image_visual, "radius_mm: " + str(np.round(radius_mm, 2)),
                   (10, height_cropped - 40), font, 1,
                   (255, 0, 255), 2, cv.LINE_AA)
        cv.putText(image_visual, "confidence: " + str(np.round(confidence, 2)),
                   (10, height_cropped - 10), font, 1,
                   (255, 0, 255), 2, cv.LINE_AA)
        visualize(image_visual, title="Area")
    """"""
    if generate_image and return_image is True:
        return image_visual
    else:
        return x_mm, y_mm, radius_mm, confidence


def resize_to_predefined(img_input):
    width = 1280
    height = 960
    dim = (width, height)

    # resize image
    return cv.resize(img_input, dim, interpolation=cv.INTER_AREA)


def compute_confidence(mark_center_x, mark_center_y, center_x, center_y, radius_):
    # confidence for the radius
    radius_shift = [np.pi * np.maximum(np.minimum(radius_ / mark_center_x, 1.), 0)]
    confidence_radius = smooth_wave_filter(radius_shift, steepness=.3)

    # confidence for the center
    max_radius_x = mark_center_x - radius_
    shift_x = np.minimum(np.absolute(center_x - mark_center_x) / (2 * max_radius_x) + .5, 1.)
    # confidence_center_x = smooth_wave_filter([np.pi * shift_x], steepness=.1)

    new_mark_center_y = (mark_center_y + radius_) / 2
    max_radius_y = new_mark_center_y - radius_
    shift_y = np.minimum(np.absolute(center_y - new_mark_center_y) / (2 * max_radius_y) + .5, 1.)
    # confidence_center_y = smooth_wave_filter([np.pi * shift_y], steepness=.1)

    # return confidence_radius * confidence_center_x * confidence_center_y * 100
    confidence_center = smooth_wave_filter([np.pi * np.sqrt((shift_x ** 2 + shift_y ** 2) / 2)], steepness=.1)
    # print(confidence_radius, confidence_center)
    return confidence_radius * confidence_center * 100


if __name__ == '__main__':  # avoid import and run ensure file is run directly in python
    # image read
    img_input = cv.imread('new_objective_5_low.jpg')  # update with camera input

    for _ in range(4):
        storage_id = str(_ + 1)
        camera_hostname = ""
        camera_id = ""

        x_mm, y_mm, radius_mm, confidence = find_area(storage_id, img_input)
        print("-------------------")
        print("xcoordinate", x_mm,
              "ycoordinate", y_mm,
              "radius", radius_mm,
              "confidence", confidence,
              )
