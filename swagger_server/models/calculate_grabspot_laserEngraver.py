# import numpy as np
# import cv2 as cv
# from matplotlib import pyplot as plt
import math
from swagger_server.models.image_process import test_generator, save_result
from polylabel import polylabel
# from tensorflow import keras

from swagger_server.__init__ import MARKERS_LASER_ENGRAVER
from swagger_server.models.common_functions import *

MARKER_GAP = 205
TABLE_EDGE = 25
TABLE_OFFSET = 100


def get_markers(img_input):
    # splitting
    size = 300

    points = [
        [384, 656], [765, 656], [759, 994], [408, 990]  # [214, 296], [752, 281], [753, 824], [234, 812]
    ]
    return run_marker_detection(img_input=img_input, size=size, points=points)


def find_grab_spot(img_input, return_image=False):
    # variables
    # visualize(img_input)
    generate_image = SHOW_PLOTS or return_image

    storage_id = 0

    img_input = resize_to_predefined(img_input)
    img_gray = cv.cvtColor(img_input, cv.COLOR_BGR2GRAY)

    # get marker locations
    if CALIBRATION['initialCalibrate'] == "1" and len(MARKERS_LASER_ENGRAVER) > 0:
        markers = [MARKERS_LASER_ENGRAVER[0]]
    else:
        markers, delta = get_markers(img_input=img_gray)

    """
    if generate_image is True:
        img_plot = img_input.copy()
        for i in range(4):
            cv.drawMarker(img_plot, (markers[storage_id][i][0], markers[storage_id][i][1]), (0, 0, 255), thickness=4)
        visualize(img_plot, title="Markers of selected storage")
    """

    # select the correct storage area
    # Here, specify the input coordinates for corners in the order of TL, TR, BR, BL
    marker_rectangle = np.float32([markers[storage_id][0], markers[storage_id][1],
                                   markers[storage_id][2], markers[storage_id][3]])
    # print(marker_rectangle)
    # delta = 0  # += 20

    # treat camera distortions ---------------------------
    # read input width and height
    height_img, width_img = img_gray.shape[:2]
    # get top and left dimensions and set to corner_coordinates dimensions of target rectangle
    width = round(math.hypot(marker_rectangle[0, 0] - marker_rectangle[1, 0],
                             marker_rectangle[0, 1] - marker_rectangle[1, 1]))
    height = round(math.hypot(marker_rectangle[0, 0] - marker_rectangle[3, 0],
                              marker_rectangle[0, 1] - marker_rectangle[3, 1]))
    # top left coordinates
    x_top_left, y_top_left = marker_rectangle[0, 0], marker_rectangle[0, 1]
    # corner_coordinates coordinates of corners in order TL, TR, BR, BL
    corner_coordinates = np.float32([
        [x_top_left, y_top_left], [x_top_left + width - 1, y_top_left],
        [x_top_left + width - 1, y_top_left + height - 1], [x_top_left, y_top_left + height - 1]
    ])
    # print(corner_coordinates)

    # compute perspective matrix
    perspective_matrix = cv.getPerspectiveTransform(marker_rectangle, corner_coordinates)
    # Perspective transformation setting and corner_coordinates size is the same as the input image size ???
    img_straighten = cv.warpPerspective(img_gray, perspective_matrix, (width_img, height_img), cv.INTER_LINEAR,
                                        borderMode=cv.BORDER_CONSTANT, borderValue=(0, 0, 0))
    """
    if generate_image is True:
        img_straighten_color = cv.warpPerspective(img_input, perspective_matrix, (width_img, height_img),
                                                  cv.INTER_LINEAR, borderMode=cv.BORDER_CONSTANT, borderValue=(0, 0, 0))
        visualize(img_straighten_color, title="Distortion correction")
    """

    edges, \
        new_markers, \
        img_cropped,\
        kernel_dilation = workpiece_isolation(
            markers=markers,
            perspective_matrix=perspective_matrix,
            img_straighten=img_straighten
        )

    # making a square from markers
    width_cropped = np.minimum(new_markers[1][0] - new_markers[0][0], new_markers[2][0] - new_markers[3][0])
    # Calculate scaling from pixels to actual distances
    scale_factor = MARKER_GAP / width_cropped
    height_markers = np.minimum(new_markers[3][1] - new_markers[0][1], new_markers[2][1] - new_markers[1][1])
    height_cropped = int(round(img_cropped.shape[0] * width_cropped / height_markers))
    img_cropped = cv.resize(img_cropped, (width_cropped, height_cropped), interpolation=cv.INTER_AREA)
    markers_cropped = [
        [0, height_cropped - width_cropped - 1], [width_cropped - 1, height_cropped - width_cropped - 1],
        [0, height_cropped - 1], [width_cropped - 1, height_cropped - 1]
    ]

    contour_areas, polygon_approx, contours_all = edge_to_area(img_cropped=img_cropped, kernel_dilation=kernel_dilation)

    # sorting area indexes
    sorted_indices_area = np.argsort(contour_areas)
    # print(len(polygon_approx[sorted_indices_area[-1]]), len(polygon_approx[sorted_indices_area[-2]]))

    this_contour = contours_all[sorted_indices_area[-2]]
    # this_polygon = np.squeeze(polygon_approx[sorted_indices_area[-2]])

    best_grab_point_coordinates, best_grab_point_angle = draw_and_compute(
        this_contour, img_cropped, markers_cropped, scale_factor)

    # Measurements for confidence calculations
    idx = np.argwhere(this_contour[:, :, 1] < markers_cropped[0][1] - int(TABLE_EDGE / scale_factor))
    max_deflect, distance_to_hull = manipulate_for_confidence(
        img_cropped, this_contour, best_grab_point_coordinates, idx)

    # translate to the original coordinates
    x_mm = 1 * MARKER_GAP - (storage_id * MARKER_GAP + pixel_to_mm(best_grab_point_coordinates[1], ratio=scale_factor))
    y_mm = pixel_to_mm(markers_cropped[3][1] - best_grab_point_coordinates[0], ratio=scale_factor)
    angle_deg = np.rad2deg(best_grab_point_angle)
    confidence = compute_confidence(MARKER_GAP + TABLE_EDGE, y_mm,
                                    pixel_to_mm(max_deflect / 255, ratio=scale_factor),
                                    pixel_to_mm(distance_to_hull, ratio=scale_factor))
    """"""
    # extracted contour, visualization only
    if generate_image is True:
        img_plot = img_input.copy()
        for i in range(4):
            cv.drawMarker(img_plot, (markers[storage_id][i][0], markers[storage_id][i][1]), (0, 0, 255), thickness=4)
        visualize(img_plot, title="Markers of selected storage")

        img_straighten_color = cv.warpPerspective(img_input, perspective_matrix, (width_img, height_img),
                                                  cv.INTER_LINEAR, borderMode=cv.BORDER_CONSTANT, borderValue=(0, 0, 0))
        visualize(img_straighten_color, title="Distortion correction")
        # visualize(edges, title="Edge detection")

        image_visual = img_straighten_color[
                       :new_markers[3][1],
                       new_markers[3][0]:new_markers[2][0]
                       ]
        image_visual = cv.resize(image_visual, (width_cropped, height_cropped), interpolation=cv.INTER_AREA)
        for i in range(4):
            cv.drawMarker(image_visual, (markers_cropped[i][0], markers_cropped[i][1]), (0, 0, 255), thickness=4)
        cv.line(image_visual, (best_grab_point_coordinates[1], best_grab_point_coordinates[0]),
                (best_grab_point_coordinates[1], best_grab_point_coordinates[0] - int(width_cropped / 8)),
                (0, 255, 0), thickness=4)
        cv.line(image_visual, (best_grab_point_coordinates[1], best_grab_point_coordinates[0]),
                (best_grab_point_coordinates[1] - int(np.sin(best_grab_point_angle) * width_cropped / 8),
                 best_grab_point_coordinates[0] - int(np.cos(best_grab_point_angle) * width_cropped / 8)),
                (0, 255, 0), thickness=4)
        cv.drawMarker(image_visual, (best_grab_point_coordinates[1], best_grab_point_coordinates[0]),
                      (0, 0, 255), thickness=4)
        # cv.circle(image_visual, (int(centre[0]), int(centre[1])), int(radius), (34, 139, 34), -1)
        # show information on image
        font = cv.FONT_HERSHEY_SIMPLEX
        str(np.round(x_mm, 2))
        cv.putText(image_visual, "x_mm: " + str(np.round(x_mm, 2)),
                   (10, height_cropped - 100), font, 1,
                   (255, 0, 255), 2, cv.LINE_AA)
        cv.putText(image_visual, "y_mm: " + str(np.round(y_mm, 2)),
                   (10, height_cropped - 70), font, 1,
                   (255, 0, 255), 2, cv.LINE_AA)
        cv.putText(image_visual, "angle_deg: " + str(np.round(angle_deg, 2)),
                   (10, height_cropped - 40), font, 1,
                   (255, 0, 255), 2, cv.LINE_AA)
        cv.putText(image_visual, "confidence: " + str(np.round(confidence, 2)),
                   (10, height_cropped - 10), font, 1,
                   (255, 0, 255), 2, cv.LINE_AA)
        visualize(image_visual, title="Grab Spot")
        # cv.imwrite('temp_x.jpg', image_visual)
    """"""
    if generate_image and return_image is True:
        return image_visual
    else:
        return x_mm, y_mm, angle_deg, confidence


def manipulate_for_confidence(img_cropped, this_contour, best_grab_point_coordinates, idx):
    z = np.zeros(img_cropped.shape)
    cv.drawContours(z, [this_contour], -1, (128, 128, 128), thickness=cv.FILLED)
    cv.drawContours(z, [this_contour], -1, (0, 0, 0), thickness=10)
    grab_pnt = (int(best_grab_point_coordinates[1]), int(best_grab_point_coordinates[0]))
    # convex_hull = cv.convexHull(temp_c)
    # print(convex_hull.shape)
    # cv.drawContours(z, convex_hull, -1, (0, 128, 0), 10)
    convex_hull = cv.convexHull(this_contour, returnPoints=False)
    defects = cv.convexityDefects(this_contour, convex_hull)
    # print(defects.shape)
    s_, e_, f_, d_ = defects[0, 0]
    start = tuple(this_contour[s_][0])
    end = tuple(this_contour[e_][0])
    # far = tuple(this_contour[f_][0])
    # distance = d_ / 256.0
    cv.line(z, start, end, (255, 255, 255), 2)
    defects_filtered = []
    this_contour_filtered = []
    for i in range(defects.shape[0]):
        s, e, f, d = defects[i, 0]
        if s in idx[:, 0] and e in idx[:, 0]:
            if s != e_:
                start = tuple(this_contour[e_][0])
                this_contour_filtered.append([this_contour[e_][0]])
                end = tuple(this_contour[s][0])
                cv.line(z, start, end, (255, 255, 255), 2)
            start = tuple(this_contour[s][0])
            this_contour_filtered.append([this_contour[s][0]])
            end = tuple(this_contour[e][0])
            # far = tuple(this_contour[f][0])
            # distance = d / 256.0
            cv.line(z, start, end, (255, 255, 255), 2)

            defects_filtered.append([s, e, f, d])
        # s_ = s
        e_ = e
        # f_ = f
        # d_ = d
        # cv.circle(z, far, 5, (0, 0, 255), -1)
    this_contour_filtered.append([this_contour[e][0]])
    cv.imwrite('temp_1.jpg', z)
    this_contour_filtered = np.array(this_contour_filtered)
    defects_filtered = np.array(defects_filtered)
    dist_to_hull = cv.pointPolygonTest(this_contour_filtered, grab_pnt, True)
    print('Max defect:', np.max(defects_filtered[:, 3]))
    print('Distance to hull:', dist_to_hull)
    return np.max(defects_filtered[:, 3]), dist_to_hull


def draw_and_compute(this_contour, img_cropped, markers_cropped, scale_factor):
    angle, centre, ratio = run_pca(this_contour)
    # angle = 0.
    holder_0, holder_1 = np.zeros(img_cropped.shape), np.zeros(img_cropped.shape)
    cv.drawContours(holder_0, [this_contour], -1, (128, 128, 128), thickness=cv.FILLED)
    holder_x = holder_0.copy()
    cv.drawContours(holder_0, this_contour, -1, (0, 0, 0), thickness=int(np.round(10 / scale_factor)))
    cv.drawContours(holder_x, this_contour, -1, (0, 0, 0), thickness=int(np.round(10 / scale_factor) + 2))
    holder_0 = holder_0 - holder_x
    # cv.drawContours(holder_0, [this_contour], -1, (128, 128, 128), thickness=cv.FILLED)
    line_length = 500
    # num_steps = 8  # 6 * 4
    # num_degree = 3
    num_range = np.pi / 3
    num_shift = - np.pi / 2
    num_array = np.array([-55, -34, -21, -13, -8, -5, -3, -2, -1, 0, 1, 2, 3, 5, 8, 13, 21, 34, 55]) * np.pi / (2 * 55)
    angle_init = angle - np.pi  # -np.pi/2
    angle_range = angle_init + num_array
    angle_range = angle_range[num_shift - num_range <= angle_range]
    angle_range = angle_range[angle_range <= num_shift + num_range]
    # angle_range = angle_range + num_shift
    for angle_ in angle_range:
        point_ = (int(centre[0] + line_length * np.cos(angle_)), int(centre[1] + line_length * np.sin(angle_)))
        cv.line(holder_1, centre, point_, (128, 128, 128), 2)

    """for i in range(num_steps):
        angle_ = angle + i * 2 * np.pi / num_steps
        point_ = (int(centre[0] + line_length * np.cos(angle_)), int(centre[1] + line_length * np.sin(angle_)))
        cv.line(holder_1, centre, point_, (128, 128, 128), 2)
    """
    # Finding grab spot
    holder_1 = holder_0 + holder_1
    holder_0[(markers_cropped[0][1] - int(TABLE_EDGE / scale_factor)):, :] = 0
    holder_1[(markers_cropped[0][1] - int(TABLE_EDGE / scale_factor)):, :] = 0
    cv.imwrite('temp_0.jpg', holder_1)
    grab_points = np.argwhere(holder_1 > 200)
    distance_grab_points = np.linalg.norm(grab_points, axis=1)
    list_grab_points = []
    list_grab_point_angles = []
    list_distances = []
    last_i = 0
    i = 0
    assert distance_grab_points.shape[0] > 0, (400, 'Insufficient area to grab.')
    last_distance = distance_grab_points[i]
    distance_centre_to_grab_point = []
    vector_centre_to_grab_point = []
    for i in range(1, distance_grab_points.shape[0]):
        if not np.abs(last_distance - distance_grab_points[i]) <= 3:
            list_distances.append(np.average(distance_grab_points[last_i:i]))
            list_grab_points.append(np.average(grab_points[last_i:i], axis=0).astype(int))
            vector_centre_to_grab_point.append(list_grab_points[-1] - np.array([centre[1], centre[0]]))
            distance_centre_to_grab_point.append(np.linalg.norm(vector_centre_to_grab_point[-1]))
            list_grab_point_angles.append(get_angle(vector_centre_to_grab_point[-1]))
            last_i = i
            last_distance = distance_grab_points[i]
    list_distances.append(np.average(distance_grab_points[last_i:(i + 1)]))
    list_grab_points.append(np.average(grab_points[last_i:(i + 1)], axis=0).astype(int))
    vector_centre_to_grab_point.append(list_grab_points[-1] - np.array([centre[1], centre[0]]))
    distance_centre_to_grab_point.append(np.linalg.norm(vector_centre_to_grab_point[-1]))
    list_grab_point_angles.append(get_angle(vector_centre_to_grab_point[-1]))
    # Pick long length
    best_grab_point_index = pick_best_index(np.argmax(distance_centre_to_grab_point),
                                            np.argmin(np.abs(list_grab_point_angles)), curvature_ratio=ratio)
    return list_grab_points[best_grab_point_index], list_grab_point_angles[best_grab_point_index]


def pick_best_index(best_long_edge, closest_to_center, curvature_ratio):
    length = np.abs(best_long_edge - closest_to_center) + 1
    feasible_indices = np.linspace(best_long_edge, closest_to_center, length)
    deg = 2
    return int(feasible_indices[np.argmin(np.abs(np.linspace(.5 ** (1/deg), .9 ** (1/deg), length) ** deg
                                                 - curvature_ratio))])


def resize_to_predefined(img_input):
    width = 1280
    height = 1024
    dim = (width, height)

    # resize image
    return cv.resize(img_input, dim, interpolation=cv.INTER_AREA)


def compute_confidence(mark_y, grab_y, max_deflect, gap_hull):
    # confidence for the y offset
    max_radius_y = MARKER_GAP / 2
    shift_y = np.minimum(np.absolute(grab_y - mark_y) / (2 * max_radius_y) + .5, 1.)
    confidence_y = smooth_wave_filter([np.pi * shift_y], steepness=.1)

    # confidence for gap between hull and grab point
    shift_gap = np.minimum(np.maximum(gap_hull, 0), 45) / 45
    confidence_gap = smooth_wave_filter([np.pi * shift_gap], steepness=.05)

    # confidence for non-convexity
    shift_deflect = np.minimum(max_deflect / 50 + .5, 1)
    confidence_deflect = smooth_wave_filter([np.pi * shift_deflect], steepness=.1)

    return confidence_y * confidence_gap * confidence_deflect * 100


def smooth_wave_filter(input_value, steepness=0.1):
    deg_1 = 1.
    deg_2 = 1.
    for _ in input_value:
        deg_1 = deg_1 * np.sin(_)
        deg_2 = deg_2 * np.sin(_) * np.sin(_)
    return deg_1 * np.sqrt(1 + steepness) / np.sqrt(deg_2 + steepness)


def run_pca(contour):
    # Construct a buffer used by the pca analysis
    sz = len(contour)
    data_points = np.empty((sz, 2), dtype=np.float64)
    for i in range(data_points.shape[0]):
        data_points[i, 0] = contour[i, 0, 0]
        data_points[i, 1] = contour[i, 0, 1]

    # Perform PCA analysis
    mean = np.empty(0)
    mean, eigenvectors, eigenvalues = cv.PCACompute2(data_points, mean)

    # Store the center of the object
    center = (int(mean[0, 0]), int(mean[0, 1]))

    d1 = np.linalg.norm([0.02 * eigenvectors[0, 0] * eigenvalues[0, 0],
                         0.02 * eigenvectors[0, 1] * eigenvalues[0, 0]])
    d2 = np.linalg.norm([- 0.02 * eigenvectors[1, 0] * eigenvalues[1, 0],
                         - 0.02 * eigenvectors[1, 1] * eigenvalues[1, 0]])

    print('Ratio:', np.minimum(d1 / d2, d2 / d1))

    angle = np.arctan2(eigenvectors[0, 1], eigenvectors[0, 0])  # orientation in radians
    if angle < 0:
        angle = angle + np.pi

    return angle, center, np.minimum(d1 / d2, d2 / d1)


def get_angle(vector_):
    return np.arctan(np.arctan(vector_[1] / vector_[0]))  # in radians


if __name__ == '__main__':  # avoid import and run ensure file is run directly in python
    # image read
    img_input_ = cv.imread('new_objective_5_low.jpg')  # update with camera input

    camera_hostname = ""
    camera_id = ""

    # x_mm, y_mm, radius_mm, confidence
    SHOW_PLOTS = True
    img_output = find_grab_spot(img_input_, True)
    # print("-------------------")
    """
    print("xcoordinate", x_mm,
          "ycoordinate", y_mm,
          "radius", radius_mm,
          "confidence", confidence,
          )
    """
    visualize(img_output, title="Image")
