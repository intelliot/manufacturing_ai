# import numpy as np
# import cv2 as cv
# from matplotlib import pyplot as plt
import math
# from swagger_server.models.image_process import test_generator, save_result
# from polylabel import polylabel
# from tensorflow import keras

# from swagger_server.__init__ import MARKER_MODEL_INIT
from swagger_server.models.common_functions import *

MARKER_GAP = 140
TABLE_EDGE = -MARKER_GAP
WIDTH_TO_GROOVE = 120
TABLE_OFFSET = 100
CROP_OFFSET_SCALE = .25


def get_markers(img_input):
    # splitting
    size = 300

    points = [
        [409, 169], [1015, 153], [1034, 457], [410, 474]
    ]
    return run_marker_detection(img_input=img_input, size=size, points=points)


def find_grab_spot(img_input, return_image=False):
    # variables
    # visualize(img_input)
    generate_image = SHOW_PLOTS or return_image

    storage_id = 0
    kernel_dilation = np.ones((10, 10), np.uint16)  # dilation kernel_dilation

    img_input = resize_to_predefined(img_input)
    img_gray = cv.cvtColor(img_input, cv.COLOR_BGR2GRAY)

    # get marker locations
    markers, delta = get_markers(img_gray)

    """
    if generate_image is True:
        img_plot = img_input.copy()
        for i in range(4):
            cv.drawMarker(img_plot, (markers[storage_id][i][0], markers[storage_id][i][1]), (0, 0, 255), thickness=4)
        visualize(img_plot, title="Markers of selected storage")
    """

    # select the correct storage area
    # Here, specify the input coordinates for corners in the order of TL, TR, BR, BL
    marker_rectangle = np.float32([markers[0][0], markers[0][1],
                                   markers[0][2], markers[0][3]])
    # print(marker_rectangle)
    # delta = 0  # += 20

    # treat camera distortions ---------------------------
    # read input width and height
    height_img, width_img = img_gray.shape[:2]
    # get top and left dimensions and set to corner_coordinates dimensions of target rectangle
    width = round(math.hypot(marker_rectangle[0, 0] - marker_rectangle[1, 0],
                             marker_rectangle[0, 1] - marker_rectangle[1, 1]))
    height = round(math.hypot(marker_rectangle[0, 0] - marker_rectangle[3, 0],
                              marker_rectangle[0, 1] - marker_rectangle[3, 1]))
    # top left coordinates
    x_top_left, y_top_left = marker_rectangle[0, 0], marker_rectangle[0, 1]
    # corner_coordinates coordinates of corners in order TL, TR, BR, BL
    corner_coordinates = np.float32([
        [x_top_left, y_top_left], [x_top_left + width - 1, y_top_left],
        [x_top_left + width - 1, y_top_left + height - 1], [x_top_left, y_top_left + height - 1]
    ])
    # print(corner_coordinates)

    # compute perspective matrix
    perspective_matrix = cv.getPerspectiveTransform(marker_rectangle, corner_coordinates)
    # Perspective transformation setting and corner_coordinates size is the same as the input image size ???
    img_straighten = cv.warpPerspective(img_gray, perspective_matrix, (width_img, height_img), cv.INTER_LINEAR,
                                        borderMode=cv.BORDER_CONSTANT, borderValue=(0, 0, 0))
    """
    if generate_image is True:
        img_straighten_color = cv.warpPerspective(img_input, perspective_matrix, (width_img, height_img),
                                                  cv.INTER_LINEAR, borderMode=cv.BORDER_CONSTANT, borderValue=(0, 0, 0))
        visualize(img_straighten_color, title="Distortion correction")
    """

    new_markers = get_translated_points(markers[0], perspective_matrix)

    # filtering
    img_blurred = cv.bilateralFilter(img_straighten, 8, 25, 25)
    img_blurred_final = cv.GaussianBlur(img_blurred, (3, 3), 0)

    # fix brightness
    bright_sigma = 0.33
    image_median = np.median(img_blurred_final)
    if image_median > 191:  # light images
        brightness_tuner = 2
    elif image_median > 127:
        brightness_tuner = 1
    elif image_median < 63:  # dark
        brightness_tuner = 2
    else:
        brightness_tuner = 1
    brightness_lower = int(max(0, (1.0 - brightness_tuner * bright_sigma) * image_median))
    brightness_upper = int(min(255, (1.0 + brightness_tuner * bright_sigma) * image_median))

    # find edges
    edges = cv.Canny(img_blurred_final, brightness_lower, brightness_upper)
    img_dilated = cv.dilate(edges, kernel_dilation, iterations=1)

    # visualize(edges, title="Edge detection")

    width_cropped = np.minimum(new_markers[1][0] - new_markers[0][0], new_markers[2][0] - new_markers[3][0])

    crop_offset = int(CROP_OFFSET_SCALE * width_cropped)
    img_cropped = img_dilated[
                  :new_markers[3][1] + crop_offset,
                  new_markers[3][0]:new_markers[2][0]
                  ]

    # making a rectangle from markers
    scale_factor = 2 * MARKER_GAP / width_cropped  # need to fix, 280 mm
    height_markers = np.minimum(new_markers[3][1] - new_markers[0][1], new_markers[2][1] - new_markers[1][1])
    height_cropped = int(round(img_cropped.shape[0] * width_cropped / (2 * height_markers)))
    new_offset = int(round(crop_offset * width_cropped / (2 * height_markers)))
    img_cropped = cv.resize(img_cropped, (width_cropped, height_cropped), interpolation=cv.INTER_AREA)
    markers_cropped = [
        [0, height_cropped - width_cropped // 2 - new_offset - 1],
        [width_cropped - 1, height_cropped - width_cropped // 2 - new_offset - 1],
        [0, height_cropped - new_offset - 1], [width_cropped - 1, height_cropped - new_offset - 1]
    ]

    img_morphed = cv.morphologyEx(img_cropped, cv.MORPH_CLOSE, kernel_dilation)
    # get contours
    (contours_all, _) = cv.findContours(img_morphed.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

    contours = {}
    contour_areas = []
    polygon_approx = []

    for index, contour in enumerate(contours_all):
        contour_areas.append(cv.contourArea(contour))
        perimeter = cv.arcLength(contour, True)
        # approximate to a polygon with given epsilon value
        polygon_approx.append(
            cv.approxPolyDP(contours_all[index], perimeter * 0.001, True))
        contours[index] = {
            "contour": contour,
            "area": contour_areas[index],
            "perimeter": perimeter,
            "polygon_approx": polygon_approx[index],
            "approxLen": len(polygon_approx)
        }

    # sorting area indexes
    sorted_indices_area = np.argsort(contour_areas)
    # print(len(polygon_approx[sorted_indices_area[-1]]), len(polygon_approx[sorted_indices_area[-2]]))

    this_contour = contours_all[sorted_indices_area[-2]]
    # this_polygon = np.squeeze(polygon_approx[sorted_indices_area[-2]])

    best_grab_point_coordinates = draw_and_compute(
        this_contour, img_cropped, markers_cropped, scale_factor)

    # Measurements for confidence calculations
    idx0 = np.argwhere(this_contour[:, :, 1] < markers_cropped[0][1] - int(TABLE_EDGE / scale_factor))
    idx1 = np.argwhere(int(WIDTH_TO_GROOVE / scale_factor) < this_contour[:, :, 0])
    idx2 = np.argwhere(this_contour[:, :, 0] < int((2 * MARKER_GAP - WIDTH_TO_GROOVE) / scale_factor))
    idx = []
    for line in range(this_contour.shape[0]):
        if line in idx0[:, 0] and line in idx1[:, 0] and line in idx2[:, 0]:
            idx.append([line, 0])
    idx = np.array(idx)
    max_deflect, distance_to_hull = manipulate_for_confidence(
        img_cropped, this_contour, best_grab_point_coordinates, idx)

    # translate to the original coordinates
    x_mm = 2 * MARKER_GAP - pixel_to_mm(best_grab_point_coordinates[1], ratio=scale_factor)
    y_mm = pixel_to_mm(markers_cropped[3][1] - best_grab_point_coordinates[0], ratio=scale_factor)
    angle_deg = np.rad2deg(0.)
    confidence = compute_confidence(pixel_to_mm(max_deflect / 255, ratio=scale_factor),
                                    pixel_to_mm(distance_to_hull, ratio=scale_factor))
    """"""
    # extracted contour, visualization only
    if generate_image is True:
        img_plot = img_input.copy()
        for i in range(4):
            cv.drawMarker(img_plot, (markers[storage_id][i][0], markers[storage_id][i][1]), (0, 0, 255), thickness=4)
        visualize(img_plot, title="Markers of selected storage")

        img_straighten_color = cv.warpPerspective(img_input, perspective_matrix, (width_img, height_img),
                                                  cv.INTER_LINEAR, borderMode=cv.BORDER_CONSTANT, borderValue=(0, 0, 0))
        visualize(img_straighten_color, title="Distortion correction")
        # visualize(edges, title="Edge detection")

        image_visual = img_straighten_color[
                       :new_markers[3][1] + crop_offset,
                       new_markers[3][0]:new_markers[2][0]
                       ]
        image_visual = cv.resize(image_visual, (width_cropped, height_cropped), interpolation=cv.INTER_AREA)
        for i in range(4):
            cv.drawMarker(image_visual, (markers_cropped[i][0], markers_cropped[i][1]), (0, 0, 255), thickness=4)
        cv.line(image_visual, (best_grab_point_coordinates[1], best_grab_point_coordinates[0]),
                (best_grab_point_coordinates[1], best_grab_point_coordinates[0] - int(width_cropped / 8)),
                (0, 255, 0), thickness=4)
        cv.line(image_visual, (best_grab_point_coordinates[1], best_grab_point_coordinates[0]),
                (best_grab_point_coordinates[1] - int(np.sin(0) * width_cropped / 8),
                 best_grab_point_coordinates[0] - int(np.cos(0) * width_cropped / 8)),
                (0, 255, 0), thickness=4)
        cv.drawMarker(image_visual, (best_grab_point_coordinates[1], best_grab_point_coordinates[0]),
                      (0, 0, 255), thickness=4)
        # cv.circle(image_visual, (int(centre[0]), int(centre[1])), int(radius), (34, 139, 34), -1)
        # show information on image
        font = cv.FONT_HERSHEY_SIMPLEX
        str(np.round(x_mm, 2))
        cv.putText(image_visual, "x_mm: " + str(np.round(x_mm, 2)),
                   (10, height_cropped - 100), font, 1,
                   (255, 0, 255), 2, cv.LINE_AA)
        cv.putText(image_visual, "y_mm: " + str(np.round(y_mm, 2)),
                   (10, height_cropped - 70), font, 1,
                   (255, 0, 255), 2, cv.LINE_AA)
        cv.putText(image_visual, "angle_deg: " + str(np.round(angle_deg, 2)),
                   (10, height_cropped - 40), font, 1,
                   (255, 0, 255), 2, cv.LINE_AA)
        cv.putText(image_visual, "confidence: " + str(np.round(confidence, 2)),
                   (10, height_cropped - 10), font, 1,
                   (255, 0, 255), 2, cv.LINE_AA)
        visualize(image_visual, title="Grab Spot")
        # cv.imwrite('temp_x.jpg', image_visual)
    """"""
    if generate_image and return_image is True:
        return image_visual
    else:
        return x_mm, y_mm, angle_deg, confidence


def manipulate_for_confidence(img_cropped, this_contour, best_grab_point_coordinates, idx):
    z = np.zeros(img_cropped.shape)
    cv.drawContours(z, [this_contour], -1, (128, 128, 128), thickness=cv.FILLED)
    cv.drawContours(z, [this_contour], -1, (0, 0, 0), thickness=10)
    grab_pnt = (int(best_grab_point_coordinates[1]), int(best_grab_point_coordinates[0]))
    # convex_hull = cv.convexHull(temp_c)
    # print(convex_hull.shape)
    # cv.drawContours(z, convex_hull, -1, (0, 128, 0), 10)
    convex_hull = cv.convexHull(this_contour, returnPoints=False)
    defects = cv.convexityDefects(this_contour, convex_hull)
    # print(defects.shape)
    s_, e_, f_, d_ = defects[0, 0]
    start = tuple(this_contour[s_][0])
    end = tuple(this_contour[e_][0])
    # far = tuple(this_contour[f_][0])
    # distance = d_ / 256.0
    cv.line(z, start, end, (255, 255, 255), 2)
    defects_filtered = []
    this_contour_filtered = []
    for i in range(defects.shape[0]):
        s, e, f, d = defects[i, 0]
        if s in idx[:, 0] and e in idx[:, 0]:
            if s != e_:
                start = tuple(this_contour[e_][0])
                this_contour_filtered.append([this_contour[e_][0]])
                end = tuple(this_contour[s][0])
                cv.line(z, start, end, (255, 255, 255), 2)
            start = tuple(this_contour[s][0])
            this_contour_filtered.append([this_contour[s][0]])
            end = tuple(this_contour[e][0])
            # far = tuple(this_contour[f][0])
            # distance = d / 256.0
            cv.line(z, start, end, (255, 255, 255), 2)

            defects_filtered.append([s, e, f, d])
        # s_ = s
        e_ = e
        # f_ = f
        # d_ = d
        # cv.circle(z, far, 5, (0, 0, 255), -1)
    this_contour_filtered.append([this_contour[e][0]])
    cv.imwrite('temp_1.jpg', z)
    this_contour_filtered = np.array(this_contour_filtered)
    defects_filtered = np.array(defects_filtered)
    dist_to_hull = cv.pointPolygonTest(this_contour_filtered, grab_pnt, True)
    print('Max defect:', np.max(defects_filtered[:, 3]))
    print('Distance to hull:', dist_to_hull)
    return np.max(defects_filtered[:, 3]), dist_to_hull


def draw_and_compute(this_contour, img_cropped, markers_cropped, scale_factor):
    holder_0, holder_1 = np.zeros(img_cropped.shape), np.zeros(img_cropped.shape)
    cv.drawContours(holder_0, [this_contour], -1, (128, 128, 128), thickness=cv.FILLED)
    holder_x = holder_0.copy()
    cv.drawContours(holder_0, this_contour, -1, (0, 0, 0), thickness=int(np.round(10 / scale_factor)))
    cv.drawContours(holder_x, this_contour, -1, (0, 0, 0), thickness=int(np.round(10 / scale_factor) + 2))
    holder_0 = holder_0 - holder_x
    # cv.drawContours(holder_0, [this_contour], -1, (128, 128, 128), thickness=cv.FILLED)
    offset_range = np.array([-3, -2, -1, 0, 1, 2, 3]) * 5 / scale_factor
    for offset_ in offset_range:
        centre_ = (int(offset_ + img_cropped.shape[1] / 2), img_cropped.shape[0])
        point_ = (centre_[0], 0)
        cv.line(holder_1, centre_, point_, (128, 128, 128), 2)

    # Finding grab spot
    holder_1 = holder_0 + holder_1
    holder_0[(markers_cropped[0][1] - int(TABLE_EDGE / scale_factor)):, :] = 0
    holder_1[(markers_cropped[0][1] - int(TABLE_EDGE / scale_factor)):, :] = 0
    cv.imwrite('temp_0.jpg', holder_1)
    grab_points = np.argwhere(holder_1 > 200)
    distance_grab_points = grab_points[:, 0]
    list_grab_points = []
    list_distances = []
    last_i = 0
    i = 0
    assert distance_grab_points.shape[0] > 0, (400, 'Insufficient area to grab.')
    last_x = grab_points[i, 1]
    for i in range(1, distance_grab_points.shape[0]):
        if not np.abs(last_x - grab_points[i, 1]) <= 3:
            list_distances.append(np.average(distance_grab_points[last_i:i]))
            list_grab_points.append(np.average(grab_points[last_i:i], axis=0).astype(int))
            last_i = i
            last_x = grab_points[i, 1]
    list_distances.append(np.average(distance_grab_points[last_i:(i + 1)]))
    list_grab_points.append(np.average(grab_points[last_i:(i + 1)], axis=0).astype(int))
    # Pick long length
    best_grab_point_index = np.argmin(list_distances)
    return list_grab_points[best_grab_point_index]


"""
def pick_best_index(best_long_edge, closest_to_center, curvature_ratio):
    length = np.abs(best_long_edge - closest_to_center) + 1
    feasible_indices = np.linspace(best_long_edge, closest_to_center, length)
    deg = 2
    return int(feasible_indices[np.argmin(np.abs(np.linspace(.5 ** (1/deg), .9 ** (1/deg), length) ** deg
                                                 - curvature_ratio))])
"""


def resize_to_predefined(img_input):
    width = 1280
    height = 960
    dim = (width, height)

    # resize image
    return cv.resize(img_input, dim, interpolation=cv.INTER_AREA)


def compute_confidence(max_deflect, gap_hull):
    # confidence for gap between hull and grab point
    shift_gap = np.minimum(np.maximum(gap_hull, 0), 45) / 45
    confidence_gap = smooth_wave_filter([np.pi * shift_gap], steepness=.01)

    # confidence for non-convexity
    shift_deflect = np.minimum(max_deflect / 50 + .5, 1)
    confidence_deflect = smooth_wave_filter([np.pi * shift_deflect], steepness=.1)

    return confidence_gap * confidence_deflect * 100


if __name__ == '__main__':  # avoid import and run ensure file is run directly in python
    # image read
    img_input_ = cv.imread('test_6.jpg')  # update with camera input

    camera_hostname = ""
    camera_id = ""

    # x_mm, y_mm, radius_mm, confidence
    SHOW_PLOTS = True
    img_output = find_grab_spot(img_input_, True)
    # print("-------------------")
    """
    print("xcoordinate", x_mm,
          "ycoordinate", y_mm,
          "radius", radius_mm,
          "confidence", confidence,
          )
    """
    visualize(img_output, title="Image")
