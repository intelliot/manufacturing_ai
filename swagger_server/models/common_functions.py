"""
All the functions that are common for engrave area detection and grab spot calculation are included here
"""
import cv2 as cv
import numpy as np
from polylabel import polylabel

from swagger_server.models.image_process import test_generator, save_result
from swagger_server.__init__ import SHOW_PLOTS
from swagger_server.__init__ import MARKER_MODEL_INIT, CALIBRATION

MARKER_MODEL = MARKER_MODEL_INIT


def visualize(img, title=None, height=600):
    if SHOW_PLOTS is True:
        width = int(height * img.shape[1] / img.shape[0])
        # print(height, width)
        resized = cv.resize(img, (width, height), interpolation=cv.INTER_AREA)
        if title is None:
            title = "Image"
        cv.imshow(title, resized)
        cv.waitKey(0)
        cv.destroyAllWindows()


def pixel_to_mm(length, ratio=1.):
    return length * ratio


def smooth_wave_filter(input_value, steepness=0.1):
    deg_1 = 1.
    deg_2 = 1.
    for _ in input_value:
        deg_1 = deg_1 * np.sin(_)
        deg_2 = deg_2 * np.sin(_) * np.sin(_)
    return deg_1 * np.sqrt(1 + steepness) / np.sqrt(deg_2 + steepness)


def get_angle(vector_):
    return np.arctan(np.arctan(vector_[1] / vector_[0]))  # in radians


def get_translated_points(points, matrix):
    new_points = []
    for p in points:
        px = (matrix[0][0] * p[0] + matrix[0][1] * p[1] + matrix[0][2]) / \
             (matrix[2][0] * p[0] + matrix[2][1] * p[1] + matrix[2][2])
        py = (matrix[1][0] * p[0] + matrix[1][1] * p[1] + matrix[1][2]) / \
             (matrix[2][0] * p[0] + matrix[2][1] * p[1] + matrix[2][2])
        new_points.append([int(round(px)), int(round(py))])
    return new_points


def run_marker_detection(img_input, size, points, storage_id=0):
    # splitting
    num_markers = len(points)
    segment_indices = get_segment_indices(num_markers=num_markers, storage_id=storage_id)
    img_segments = []
    segment_origin = []
    for _ in range(4):
        idx = segment_indices[_]
        this_segment = img_input[
                max(int(points[idx][1] - size / 2), 0): min(int(points[idx][1] + size / 2), img_input.shape[0]),
                max(int(points[idx][0] - size / 2), 0): min(int(points[idx][0] + size / 2), img_input.shape[1])]
        brightness_scale = np.sqrt(1598.99 / np.var(this_segment))
        brightness_shift = 133.11 - brightness_scale * np.mean(this_segment)
        this_segment = brightness_scale * this_segment + brightness_shift
        img_segments.append(this_segment)
        segment_origin.append(
            [max(int(points[idx][1] - size / 2), 0), max(int(points[idx][0] - size / 2), 0)])
    """
    img_segments = []
    segment_origin = []
    for idx in range(num_markers):
        img_segments.append(
            img_input[
                max(int(points[idx][1] - size / 2), 0): min(int(points[idx][1] + size / 2), img_input.shape[0]),
                max(int(points[idx][0] - size / 2), 0): min(int(points[idx][0] + size / 2), img_input.shape[1])]
        )
        segment_origin.append(
            [max(int(points[idx][1] - size / 2), 0), max(int(points[idx][0] - size / 2), 0)])
    """
    # visualize
    """
    if GENERATE_IMAGE is True:
        combined_segments = np.vstack((
            np.hstack(tuple(map(tuple, img_segments[:5]))),
            np.hstack(tuple(map(tuple, img_segments[5:])))
        ))
        visualize(combined_segments, title="Original segments")
    """
    # converting images into marker detected images
    """"""
    input_images = test_generator(img_segments)
    output_images = MARKER_MODEL.predict(input_images, len(img_segments), verbose=1)
    images_marker = save_result(output_images, flag_multi_class=False)
    # np.save('marker_images.npy', images_marker)
    """"""
    # extracting location of the centre_ of the markers
    centers_local = []
    centers_global = []
    # images_marker = np.load('marker_images.npy')
    # print(images_marker)
    for indMarkers in range(len(images_marker)):
        segment_input = images_marker[indMarkers]
        segment_input = np.transpose(segment_input)
        segment_input = cv.normalize(segment_input, None, 0, 255, cv.NORM_MINMAX, cv.CV_8U)
        segment_input = cv.resize(segment_input, img_segments[indMarkers].shape, interpolation=cv.INTER_AREA)
        # img = cv.rotate(img, cv.ROTATE_90_COUNTERCLOCKWISE)
        """
        filename = "marker_" + str(indMarkers) + ".png"
        matplotlib.image.imsave(filename, img, cmap='gray')
        """

        (contours_, _) = cv.findContours(segment_input, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        areas_ = []
        poly_approx_ = []
        for i, contour in enumerate(contours_):
            areas_.append(cv.contourArea(contour))
            perimeter = cv.arcLength(contour, True)
            poly_approx_.append(cv.approxPolyDP(contours_[i], perimeter * 0.001, True))
        assert len(areas_) > 0, (400, 'Missing marker(s) from the camera image')
        sorted_area_index = np.argsort(areas_)
        poly_ = np.squeeze(poly_approx_[sorted_area_index[-1]])
        if poly_.shape[0] > 2:
            centre_, radius_ = polylabel([poly_.tolist()], with_distance=True)
            c_local = [int(centre_[1]), int(centre_[0])]
            c_global = [c_local[0] + segment_origin[indMarkers][1],
                        c_local[1] + segment_origin[indMarkers][0]]
            centers_local.append(c_local)
            centers_global.append(c_global)
    # visualize
    """
    if GENERATE_IMAGE is True:
        img_plot = img_input.copy()
        for i in range(10):
            cv.drawMarker(img_plot, (centers_global[i][0], centers_global[i][1]), (255, 0, 0), thickness=2)
        visualize(img_plot, title="Marked full image")
    """

    # marker_corners = update_markers(points=centers_global, num_markers=num_markers)
    marker_corners = [centers_global]
    assert len(marker_corners) > 0, (405, 'Missing marker(s) from detection')
    marker_radius = 0  # 2018 - 1955
    return marker_corners, marker_radius


def update_markers(points, num_markers):
    if num_markers == 10:
        markers = [
            [points[0], points[1], points[8], points[9]],  # corners in order TL, TR, BR, BL
            [points[1], points[2], points[7], points[8]],  # corners in order TL, TR, BR, BL
            [points[2], points[3], points[6], points[7]],  # corners in order TL, TR, BR, BL
            [points[3], points[4], points[5], points[6]]  # corners in order TL, TR, BR, BL
        ]
    elif num_markers == 4:
        markers = [
            [points[0], points[1], points[2], points[3]]  # corners in order TL, TR, BR, BL
        ]
    else:
        markers = []
    return markers


def get_segment_indices(num_markers, storage_id):
    if num_markers == 10:
        marker_indices = [
            [0, 1, 8, 9],  # corners in order TL, TR, BR, BL
            [1, 2, 7, 8],  # corners in order TL, TR, BR, BL
            [2, 3, 6, 7],  # corners in order TL, TR, BR, BL
            [3, 4, 5, 6]  # corners in order TL, TR, BR, BL
        ]
    elif num_markers == 4:
        marker_indices = [
            [0, 1, 2, 3]  # corners in order TL, TR, BR, BL
        ]
    else:
        marker_indices = [[]]
    return marker_indices[storage_id]


def run_pca(contour):
    # Construct a buffer used by the pca analysis
    sz = len(contour)
    data_points = np.empty((sz, 2), dtype=np.float64)
    for i in range(data_points.shape[0]):
        data_points[i, 0] = contour[i, 0, 0]
        data_points[i, 1] = contour[i, 0, 1]

    # Perform PCA analysis
    mean = np.empty(0)
    mean, eigenvectors, eigenvalues = cv.PCACompute2(data_points, mean)

    # Store the center of the object
    center = (int(mean[0, 0]), int(mean[0, 1]))

    d1 = np.linalg.norm([0.02 * eigenvectors[0, 0] * eigenvalues[0, 0],
                         0.02 * eigenvectors[0, 1] * eigenvalues[0, 0]])
    d2 = np.linalg.norm([- 0.02 * eigenvectors[1, 0] * eigenvalues[1, 0],
                         - 0.02 * eigenvectors[1, 1] * eigenvalues[1, 0]])

    print('Ratio:', np.minimum(d1 / d2, d2 / d1))

    angle = np.arctan2(eigenvectors[0, 1], eigenvectors[0, 0])  # orientation in radians
    if angle < 0:
        angle = angle + np.pi

    return angle, center, np.minimum(d1 / d2, d2 / d1)


def run_marker_detection_init(img_input, size, points):
    # splitting
    num_markers = len(points)
    img_segments = []
    segment_origin = []
    for idx in range(num_markers):
        img_segments.append(
            img_input[
                max(int(points[idx][1] - size / 2), 0): min(int(points[idx][1] + size / 2), img_input.shape[0]),
                max(int(points[idx][0] - size / 2), 0): min(int(points[idx][0] + size / 2), img_input.shape[1])]
        )
        segment_origin.append(
            [max(int(points[idx][1] - size / 2), 0), max(int(points[idx][0] - size / 2), 0)])
    # visualize
    """
    if GENERATE_IMAGE is True:
        combined_segments = np.vstack((
            np.hstack(tuple(map(tuple, img_segments[:5]))),
            np.hstack(tuple(map(tuple, img_segments[5:])))
        ))
        visualize(combined_segments, title="Original segments")
    """
    # converting images into marker detected images
    """"""
    input_images = test_generator(img_segments)
    output_images = MARKER_MODEL.predict(input_images, len(img_segments), verbose=1)
    images_marker = save_result(output_images, flag_multi_class=False)
    # np.save('marker_images.npy', images_marker)
    """"""
    # extracting location of the centre_ of the markers
    centers_local = []
    centers_global = []
    # images_marker = np.load('marker_images.npy')
    # print(images_marker)
    for indMarkers in range(len(images_marker)):
        segment_input = images_marker[indMarkers]
        segment_input = np.transpose(segment_input)
        segment_input = cv.normalize(segment_input, None, 0, 255, cv.NORM_MINMAX, cv.CV_8U)
        segment_input = cv.resize(segment_input, img_segments[indMarkers].shape, interpolation=cv.INTER_AREA)
        # img = cv.rotate(img, cv.ROTATE_90_COUNTERCLOCKWISE)
        """
        filename = "marker_" + str(indMarkers) + ".png"
        matplotlib.image.imsave(filename, img, cmap='gray')
        """

        (contours_, _) = cv.findContours(segment_input, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        areas_ = []
        poly_approx_ = []
        for i, contour in enumerate(contours_):
            areas_.append(cv.contourArea(contour))
            perimeter = cv.arcLength(contour, True)
            poly_approx_.append(cv.approxPolyDP(contours_[i], perimeter * 0.001, True))
        assert len(areas_) > 0, (400, 'Missing marker(s) from the camera image')
        sorted_area_index = np.argsort(areas_)
        poly_ = np.squeeze(poly_approx_[sorted_area_index[-1]])
        if poly_.shape[0] > 2:
            centre_, radius_ = polylabel([poly_.tolist()], with_distance=True)
            c_local = [int(centre_[1]), int(centre_[0])]
            c_global = [c_local[0] + segment_origin[indMarkers][1],
                        c_local[1] + segment_origin[indMarkers][0]]
            centers_local.append(c_local)
            centers_global.append(c_global)
    # visualize
    """
    if GENERATE_IMAGE is True:
        img_plot = img_input.copy()
        for i in range(10):
            cv.drawMarker(img_plot, (centers_global[i][0], centers_global[i][1]), (255, 0, 0), thickness=2)
        visualize(img_plot, title="Marked full image")
    """

    marker_corners = update_markers(points=centers_global, num_markers=num_markers)
    assert len(marker_corners) > 0, (405, 'Missing marker(s) from detection')
    marker_radius = 0  # 2018 - 1955
    return marker_corners, marker_radius


def adjust_grab_spot(grab_point_px, grab_angle):
    offset_px = int(CALIBRATION['grabOffset'])
    new_grab_point_px = grab_point_px.copy()
    new_grab_point_px[1] = grab_point_px[1] - int(np.sin(grab_angle) * offset_px)
    new_grab_point_px[0] = grab_point_px[0] - int(np.cos(grab_angle) * offset_px)
    return new_grab_point_px


def workpiece_isolation(markers, perspective_matrix, img_straighten):
    new_markers = get_translated_points(markers[0], perspective_matrix)

    # filtering
    img_blurred = cv.bilateralFilter(img_straighten, 8, 25, 25)
    img_blurred_final = cv.GaussianBlur(img_blurred, (3, 3), 0)

    # fix brightness
    bright_sigma = 0.33
    image_median = np.median(img_blurred_final)
    if image_median > 191:  # light images
        brightness_tuner = 2
    elif image_median > 127:
        brightness_tuner = 1
    elif image_median < 63:  # dark
        brightness_tuner = 2
    else:
        brightness_tuner = 1
    brightness_lower = int(max(0, (1.0 - brightness_tuner * bright_sigma) * image_median))
    brightness_upper = int(min(255, (1.0 + brightness_tuner * bright_sigma) * image_median))

    # find edges
    edges = cv.Canny(img_blurred_final, brightness_lower, brightness_upper)
    kernel_dilation = np.ones((10, 10), np.uint16)  # dilation kernel_dilation
    img_dilated = cv.dilate(edges, kernel_dilation, iterations=1)

    # visualize(edges, title="Edge detection")

    img_cropped = img_dilated[
                  :new_markers[3][1],
                  new_markers[3][0]:new_markers[2][0]
                  ]

    return edges, \
        new_markers, \
        img_cropped, \
        kernel_dilation


def edge_to_area(img_cropped, kernel_dilation):
    img_morphed = cv.morphologyEx(img_cropped, cv.MORPH_CLOSE, kernel_dilation)
    # get contours
    (contours_all, _) = cv.findContours(img_morphed.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

    contours = {}
    contour_areas = []
    polygon_approx = []

    for index, contour in enumerate(contours_all):
        contour_areas.append(cv.contourArea(contour))
        perimeter = cv.arcLength(contour, True)
        # approximate to a polygon with given epsilon value
        polygon_approx.append(
            cv.approxPolyDP(contours_all[index], perimeter * 0.001, True))
        contours[index] = {
            "contour": contour,
            "area": contour_areas[index],
            "perimeter": perimeter,
            "polygon_approx": polygon_approx[index],
            "approxLen": len(polygon_approx)
        }
    return contour_areas, polygon_approx, contours_all
